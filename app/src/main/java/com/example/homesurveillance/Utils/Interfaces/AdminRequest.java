package com.example.homesurveillance.Utils.Interfaces;

import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.GetAllUserResponse;
import com.example.homesurveillance.Models.UserLoginResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AdminRequest {
    @GET("admin/getAllUser/{userID}")
    Call<GetAllUserResponse> getAllUser(@Header("Authorization") String authKey, @Path("userID") String userId);

    @FormUrlEncoded
    @POST("admin/addUser")
    Call<DefaultResponse> addUser(@Header("Authorization") String authKey, @Field("username")String username,
                                  @Field("password")String password, @Field("fullname")String fullname,
                                  @Field("role")String role);

    @FormUrlEncoded
    @PATCH("admin/editUser/{userID}")
    Call<DefaultResponse> updateUser(@Header("Authorization") String authKey, @Path("userID") String userId,
                                       @Field("password")String password, @Field("fullname")String fullname, @Field("role")String role);

    @FormUrlEncoded
    @PATCH("admin/editUser/{userID}")
    Call<UserLoginResponse> updateCurrentUser(@Header("Authorization") String authKey, @Path("userID") String userId,
                                              @Field("password")String password, @Field("fullname")String fullname);

    @DELETE("admin/deleteUser/{userID}")
    Call<DefaultResponse> deleteUser(@Header("Authorization") String authKey, @Path("userID") String userId);

}
