package com.example.homesurveillance.Utils;

import com.example.homesurveillance.Utils.Interfaces.AdminRequest;
import com.example.homesurveillance.Utils.Interfaces.SecurityRequest;
import com.example.homesurveillance.Utils.Interfaces.UserRequest;

public class RetrofitServiceUtils {
    public static UserRequest sendUserRequest() {
        return RetrofitClient.getRetrofitClient().create(UserRequest.class);
    }

    public static SecurityRequest sendSecurityRequest() {
        return RetrofitClient.getRetrofitClient().create(SecurityRequest.class);
    }

    public static AdminRequest sendAdminRequest() {
        return RetrofitClient.getRetrofitClient().create(AdminRequest.class);
    }
}
