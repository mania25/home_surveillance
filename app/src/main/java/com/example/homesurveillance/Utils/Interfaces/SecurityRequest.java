package com.example.homesurveillance.Utils.Interfaces;

import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.GetEventResponse;
import com.example.homesurveillance.Models.UserLoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SecurityRequest {
    @GET("security/getEvent/{userID}")
    Call<GetEventResponse> getSecurityEvent(@Header("Authorization") String authKey, @Path("userID") String userId);

    @FormUrlEncoded
    @PATCH("security/edit/{userID}")
    Call<UserLoginResponse> updateUser(@Header("Authorization") String authKey, @Path("userID") String userId,
                                       @Field("password")String password, @Field("fullname")String fullname);

    @FormUrlEncoded
    @POST("security/createFeedback/{eventID}")
    Call<DefaultResponse> createFeedback(@Header("Authorization") String authKey, @Path("eventID") String eventId,
                                         @Field("approver_id") String approverId, @Field("description")String description);

    @PATCH("security/finishEvent/{eventID}")
    Call<DefaultResponse> finishEvent(@Header("Authorization") String authKey, @Path("eventID") String eventId);
}
