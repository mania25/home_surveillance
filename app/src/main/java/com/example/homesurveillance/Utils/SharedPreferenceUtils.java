package com.example.homesurveillance.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtils {
    private static SharedPreferenceUtils instance = null;
    private SharedPreferences sharedPreferences;
    private String iv;

    public static SharedPreferenceUtils getInstance(Context context, String preferenceName) {
        if (instance == null) {
            instance = new SharedPreferenceUtils(context.getApplicationContext(), preferenceName);
        }
        return instance;
    }

    public SharedPreferenceUtils(Context context, String preferenceName) {
        sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    public void storeData(String key, String data) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, data);
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    public String getPreferenceData(String key) throws Exception {
        return sharedPreferences.getString(key, null);
    }

    public void removeData(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (contains(key)) {
            editor.remove(key);
            editor.apply();
        }
    }

    public void clearAllData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
