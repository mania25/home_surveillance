package com.example.homesurveillance.Utils.Interfaces;

import com.example.homesurveillance.Models.CreateEventResponse;
import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.ManageDeviceResponse;
import com.example.homesurveillance.Models.UserLoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserRequest {
    @FormUrlEncoded
    @POST("login")
    Call<UserLoginResponse> login(@Field("username")String username, @Field("password")String password, @Field("token")String token);

    @FormUrlEncoded
    @PATCH("user/edit/{userID}")
    Call<UserLoginResponse> updateUser(@Header("Authorization") String authKey, @Path("userID") String userId,
                                       @Field("password")String password, @Field("fullname")String fullname);

    @FormUrlEncoded
    @PUT("user/manageDevices/{userID}")
    Call<ManageDeviceResponse> manageDevice(@Header("Authorization") String authKey, @Path("userID") String userId,
                                            @Field("serial_no")String deviceSerial, @Field("name")String deviceName,
                                            @Field("address")String deviceAddress);

    @FormUrlEncoded
    @POST("user/createEvent")
    Call<CreateEventResponse> createEvent(@Header("Authorization") String authKey, @Field("issuer_id") String userId);

    @FormUrlEncoded
    @PATCH("user/updateEvent/{eventID}")
    Call<DefaultResponse> updateEvent(@Header("Authorization") String authKey, @Path("eventID") String userId,
                                      @Field("status")int status);
}

