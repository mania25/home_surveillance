package com.example.homesurveillance.Utils;

import com.example.homesurveillance.BuildConfig;
import com.example.homesurveillance.Config.BaseConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;

    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    public static Retrofit getRetrofitClient() {
        final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .readTimeout(9999, TimeUnit.SECONDS)
                .connectTimeout(9999, TimeUnit.SECONDS)
                .writeTimeout(9999, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(logging);
        }

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BaseConfig.getBaseUrl())
                    .client(okHttpClient.build())
                    .build();
        }

        return retrofit;
    }
}
