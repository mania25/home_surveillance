package com.example.homesurveillance.Main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.homesurveillance.Models.Device;
import com.example.homesurveillance.Models.ManageDeviceResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.Models.UserLoginResponse;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private EditText lblUsername, txtPassword, txtFullname, lblRole,
            lblDeviceSerial, txtDeviceName, txtDeviceIPAddress;
    private CardView cvUserDevice;
    private Button btnUpdate, btnCancel;

    private SharedPreferenceUtils userSharedPreference;

    private Gson gson;
    private Type type;

    private User user;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        lblUsername = (EditText)findViewById(R.id.lblUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        txtFullname = (EditText)findViewById(R.id.txtFullname);
        lblRole = (EditText)findViewById(R.id.lblRole);

        cvUserDevice = (CardView)findViewById(R.id.cvUserDevice);
        lblDeviceSerial = (EditText)findViewById(R.id.lblDeviceSerial);
        txtDeviceName = (EditText)findViewById(R.id.txtDeviceName);
        txtDeviceIPAddress = (EditText)findViewById(R.id.txtDeviceIPAddress);

        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        userSharedPreference = new SharedPreferenceUtils(ProfileActivity.this, "user");

        gson = new Gson();
        type = new TypeToken<User>() {}.getType();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            if (userSharedPreference.contains("userProfile")){
                user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), type);

                lblUsername.setText(user.getUsername());
                txtFullname.setText(user.getFullname());
                lblRole.setText(user.getRole());

                if (user.getDevice() == null) {
                    if (!user.getRole().equals("Home Owner")) {
                        cvUserDevice.setVisibility(View.GONE);
                    } else {
                        cvUserDevice.setVisibility(View.VISIBLE);
                        lblDeviceSerial.setEnabled(true);
                    }
                } else {
                    if (!user.getRole().equals("Home Owner")) {
                        cvUserDevice.setVisibility(View.GONE);
                    } else {
                        cvUserDevice.setVisibility(View.VISIBLE);

                        lblDeviceSerial.setEnabled(false);

                        lblDeviceSerial.setText(user.getDevice().getSerialNo());
                        txtDeviceName.setText(user.getDevice().getName());
                        txtDeviceIPAddress.setText(user.getDevice().getAddress());
                    }
                }
            }

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = ProgressDialog.show(ProfileActivity.this, "",
                            "Loading. Please wait...", true);

                    updateUserRequest();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateUserRequest() {
        try {
            txtFullname.setError(null);
            txtDeviceName.setError(null);
            txtDeviceIPAddress.setError(null);

            String password = txtPassword.getText().toString();
            final String fullName = txtFullname.getText().toString();
            final String deviceName = txtDeviceName.getText().toString();
            final String deviceIPAddress = txtDeviceIPAddress.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                txtPassword.setError(getString(R.string.error_invalid_password));
                focusView = txtPassword;
                cancel = true;
            }

            if (TextUtils.isEmpty(fullName)) {
                txtFullname.setError(getString(R.string.error_field_required));
                focusView = txtFullname;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(deviceName)) {
                txtDeviceName.setError(getString(R.string.error_field_required));
                focusView = txtDeviceName;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(deviceIPAddress)) {
                txtDeviceIPAddress.setError(getString(R.string.error_field_required));
                focusView = txtDeviceIPAddress;
                cancel = !cancel;
            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                dialog.show();

                Call<UserLoginResponse> updateUser = null;

                if (userSharedPreference.contains("AT")) {
                    if (user.getRole().equals("Home Owner")) {
                        updateUser = RetrofitServiceUtils.sendUserRequest().updateUser(
                                "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId(), password, fullName);
                    } else if (user.getRole().equals("Security")) {
                        updateUser = RetrofitServiceUtils.sendSecurityRequest().updateUser(
                                "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId(), password, fullName);
                    } else if (user.getRole().equals("Admin")) {
                        updateUser = RetrofitServiceUtils.sendAdminRequest().updateCurrentUser(
                                "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId(), password, fullName);
                    }

                    updateUser.enqueue(new Callback<UserLoginResponse>() {
                        @Override
                        public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                            if (response.isSuccessful()) {
                                if (user.getRole().equals("Home Owner")) {
                                    manageDeviceRequest();
                                } else {
                                    user.setFullname(txtFullname.getText().toString());

                                    userSharedPreference.storeData("userProfile", gson.toJson(user, type));

                                    Toast.makeText(ProfileActivity.this, "Your profile has been updated", Toast.LENGTH_LONG).show();

                                    finish();
                                }
                            } else {
                                dialog.dismiss();

                                Toast.makeText(ProfileActivity.this, "Failed to update data", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                            t.printStackTrace();

                            dialog.dismiss();

                            Toast.makeText(ProfileActivity.this, "Failed to update data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            dialog.dismiss();

            Toast.makeText(this, "Failed to update data", Toast.LENGTH_LONG).show();
        }
    }

    private void manageDeviceRequest() {
        try {
            Call<ManageDeviceResponse> manageDevices = null;

            if (userSharedPreference.contains("AT")) {
                manageDevices = RetrofitServiceUtils.sendUserRequest().manageDevice(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId(),
                        lblDeviceSerial.getText().toString(), txtDeviceName.getText().toString(),
                        txtDeviceIPAddress.getText().toString());

                manageDevices.enqueue(new Callback<ManageDeviceResponse>() {
                    @Override
                    public void onResponse(Call<ManageDeviceResponse> call, Response<ManageDeviceResponse> response) {
                        dialog.dismiss();

                        if (response.isSuccessful()) {
                            user.setFullname(txtFullname.getText().toString());

                            if (user.getDevice() != null) {
                                user.getDevice().setName(txtDeviceName.getText().toString());
                                user.getDevice().setAddress(txtDeviceIPAddress.getText().toString());
                            } else {
                                user.setDevice(new Device(lblDeviceSerial.getText().toString(), txtDeviceName.getText().toString(),
                                        txtDeviceIPAddress.getText().toString()));
                            }

                            userSharedPreference.storeData("userProfile", gson.toJson(user, type));

                            Toast.makeText(ProfileActivity.this, "Your profile has been updated", Toast.LENGTH_LONG).show();

                            finish();
                        } else {
                            Toast.makeText(ProfileActivity.this, "Failed to update data", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ManageDeviceResponse> call, Throwable t) {
                        t.printStackTrace();

                        dialog.dismiss();

                        Toast.makeText(ProfileActivity.this, "Failed to update data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            dialog.dismiss();

            Toast.makeText(ProfileActivity.this, "Failed to update data", Toast.LENGTH_LONG).show();

        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}
