package com.example.homesurveillance.Main.Security;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.Events;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity implements IVLCVout.Callback {
    public final static String TAG = "MainActivity";

    private SurfaceView mSurface;
    private SurfaceHolder holder;
    private TextView lblIssuerName, lblIssuedDate, lblStatus, lblFeedbackContain;
    private Button btnFinished, btnBack;

    private ProgressDialog dialog;
    private SharedPreferenceUtils userSharedPreference;

    private LibVLC libvlc;
    private MediaPlayer mMediaPlayer = null;

    private String mFilePath;
    private int mVideoWidth;
    private int mVideoHeight;

    private Gson gson;
    private Type eventType;
    private Events events;
    private Intent getIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        try {
            getIntent = getIntent();

            gson = new Gson();
            eventType = new TypeToken<Events>() {}.getType();

            events = gson.fromJson(getIntent.getExtras().getString("eventData"), eventType);

            mSurface = (SurfaceView) findViewById(R.id.videoStreamRaspi);
            lblIssuerName = (TextView)findViewById(R.id.lblIssuerName);
            lblIssuedDate = (TextView)findViewById(R.id.lblIssuedDate);
            lblStatus = (TextView)findViewById(R.id.lblStatus);
            lblFeedbackContain = (TextView)findViewById(R.id.lblFeedbackContain);

            btnFinished = (Button)findViewById(R.id.btnFinished);
            btnBack = (Button)findViewById(R.id.btnBack);

            userSharedPreference = new SharedPreferenceUtils(DetailActivity.this, "user");
            holder = mSurface.getHolder();

            if (events.getIssuerUser().getDevice() != null) {
                mFilePath = "rtsp://" + events.getIssuerUser().getDevice().getAddress() + ":8554/" + events.getIssuerUser().getDevice().getSerialNo();

                Log.d(TAG, "Playing: " + mFilePath);
            } else {
                mSurface.setVisibility(View.GONE);
            }

            lblIssuerName.setText(events.getIssuerUser().getFullname());
            lblIssuedDate.setText(events.getCreatedAt());

            lblFeedbackContain.setText(events.getDescription());

            int status = events.getStatus();

            if (status == 1) {
                lblStatus.setText("On Going");

                mSurface.setVisibility(View.VISIBLE);
                btnFinished.setVisibility(View.VISIBLE);
            } else if (status == 2) {
                lblStatus.setText("Finished");

                mSurface.setVisibility(View.GONE);
                btnFinished.setVisibility(View.GONE);
            } else if (status == 3) {
                lblStatus.setText("Cancelled");

                mSurface.setVisibility(View.GONE);
                btnFinished.setVisibility(View.GONE);
            }

            btnFinished.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    executeFinishEvent();
                }
            });

            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            mSurface = (SurfaceView) findViewById(R.id.videoStreamRaspi);
            holder = mSurface.getHolder();

            events = gson.fromJson(getIntent.getExtras().getString("eventData"), eventType);

            if (events.getIssuerUser().getDevice() != null && events.getStatus() != 2 && events.getStatus() != 3) {
                mFilePath = "rtsp://" + events.getIssuerUser().getDevice().getAddress() + ":8554/" + events.getIssuerUser().getDevice().getSerialNo();

                mSurface.setVisibility(View.VISIBLE);

                Log.d(TAG, "Playing: " + mFilePath);
                createPlayer(mFilePath);
            } else {
                mSurface.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        releasePlayer();
    }

    private void executeFinishEvent() {
        try {
            dialog = ProgressDialog.show(DetailActivity.this, "",
                    "Loading. Please wait...", true);

            Call<DefaultResponse> finishEventCall = null;

            if (userSharedPreference.contains("AT")) {
                finishEventCall = RetrofitServiceUtils.sendSecurityRequest().finishEvent(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), events.getId()
                );

                finishEventCall.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        dialog.dismiss();

                        if (response.isSuccessful()) {
                            Toast.makeText(DetailActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DetailActivity.this, "Failed to finish event", Toast.LENGTH_LONG).show();
                        }

                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        dialog.dismiss();

                        Toast.makeText(DetailActivity.this, "Failed to finish event", Toast.LENGTH_LONG).show();

                        finish();

                        t.printStackTrace();
                    }
                });
            }

        } catch (Exception ex) {
            dialog.dismiss();

            Toast.makeText(DetailActivity.this, "Failed to finish event", Toast.LENGTH_LONG).show();

            finish();

            ex.printStackTrace();
        }
    }

    /**
     * Used to set size for SurfaceView
     *
     * @param width
     * @param height
     */
    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        if (holder == null || mSurface == null)
            return;

        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;

        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);

        holder.setFixedSize(mVideoWidth, mVideoHeight);
        LayoutParams lp = mSurface.getLayoutParams();
        lp.width = w;
        lp.height = h;
        mSurface.setLayoutParams(lp);
        mSurface.invalidate();
    }

    /**
     * Creates MediaPlayer and plays video
     *
     * @param media
     */
    private void createPlayer(String media) {
        releasePlayer();
        try {
            if (media.length() > 0) {
                Toast toast = Toast.makeText(this, media, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0,
                        0);
                toast.show();
            }

            // Create LibVLC
            // TODO: make this more robust, and sync with audio demo
            ArrayList<String> options = new ArrayList<String>();
            //options.add("--subsdec-encoding <encoding>");
            options.add("--aout=opensles");
            options.add("--audio-time-stretch"); // time stretching
            options.add("-vvv"); // verbosity
            libvlc = new LibVLC(this, options);
            holder.setKeepScreenOn(true);

            // Creating media player
            mMediaPlayer = new MediaPlayer(libvlc);
            mMediaPlayer.setEventListener(mPlayerListener);

            // Seting up video output
            final IVLCVout vout = mMediaPlayer.getVLCVout();
            vout.setVideoView(mSurface);
            //vout.setSubtitlesView(mSurfaceSubtitles);
            vout.addCallback(this);
            vout.attachViews();

            Media m = new Media(libvlc, Uri.parse(media));
            mMediaPlayer.setMedia(m);
            mMediaPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error in creating player!", Toast
                    .LENGTH_LONG).show();
        }
    }

    private void releasePlayer() {
        if (libvlc == null || mMediaPlayer == null)
            return;

        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = null;

        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    /**
     * Registering callbacks
     */
    private MediaPlayer.EventListener mPlayerListener = new DetailActivity.MyPlayerListener(this);

    @Override
    public void onNewLayout(IVLCVout vout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vout) {
    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        Log.e(TAG, "Error with hardware acceleration");
        this.releasePlayer();
        Toast.makeText(this, "Error with hardware acceleration", Toast.LENGTH_LONG).show();
    }

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<DetailActivity> mOwner;

        public MyPlayerListener(DetailActivity owner) {
            mOwner = new WeakReference<DetailActivity>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            DetailActivity player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    Log.d(TAG, "MediaPlayerEndReached");
                    player.releasePlayer();
                    break;
                case MediaPlayer.Event.Playing:
                case MediaPlayer.Event.Paused:
                case MediaPlayer.Event.Stopped:
                default:
                    break;
            }
        }
    }
}
