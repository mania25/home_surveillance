package com.example.homesurveillance.Main.Admins;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homesurveillance.Main.Admins.Adapter.UserAdapter;
import com.example.homesurveillance.Main.LoginActivity;
import com.example.homesurveillance.Main.ProfileActivity;
import com.example.homesurveillance.Models.GetAllUserResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainAdminActivity extends AppCompatActivity {
    private ProgressBar loading;
    private RecyclerView rvUserList;
    private TextView lblDataNA;
    private FloatingActionButton btnAddNewUser;

    private SharedPreferenceUtils userSharedPreference;

    private Gson gson;
    private Type userProfileType, getUserResponseType;
    private User user;

    private UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);

        loading = (ProgressBar)findViewById(R.id.loading);
        rvUserList = (RecyclerView)findViewById(R.id.rvUserList);
        lblDataNA = (TextView)findViewById(R.id.lblDataNA);
        btnAddNewUser = (FloatingActionButton)findViewById(R.id.btnAddNewUser);

        rvUserList.setHasFixedSize(true);
        rvUserList.setLayoutManager(new LinearLayoutManager(this));
        userSharedPreference = new SharedPreferenceUtils(this, "user");

        gson = new Gson();

        userProfileType = new TypeToken<User>(){}.getType();
        getUserResponseType = new TypeToken<GetAllUserResponse>(){}.getType();

        getAdminAllUserRequest();

        btnAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextIntent = new Intent(MainAdminActivity.this, UserProfileManageActivity.class);
                startActivity(nextIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_owner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_user_logout:
                userSharedPreference.clearAllData();
                FirebaseMessaging.getInstance().unsubscribeFromTopic("security");

                Intent nextIntent = new Intent(MainAdminActivity.this, LoginActivity.class);
                startActivity(nextIntent);

                finish();
                break;
            case R.id.action_user_profile:
                nextIntent = new Intent(MainAdminActivity.this, ProfileActivity.class);
                startActivity(nextIntent);

                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAdminAllUserRequest();
    }

    private void getAdminAllUserRequest() {
        try {
            loading.setVisibility(View.VISIBLE);
            rvUserList.setVisibility(View.GONE);

            Call<GetAllUserResponse> getAllUserResponseCall = null;

            user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), userProfileType);

            if (userSharedPreference.contains("AT")) {
                getAllUserResponseCall = RetrofitServiceUtils.sendAdminRequest().getAllUser(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId());

                getAllUserResponseCall.enqueue(new Callback<GetAllUserResponse>() {
                    @Override
                    public void onResponse(Call<GetAllUserResponse> call, Response<GetAllUserResponse> response) {
                        loading.setVisibility(View.GONE);

                        if (response.isSuccessful()) {
                            if (response.body().getData().size() > 0) {
                                rvUserList.setVisibility(View.VISIBLE);
                                lblDataNA.setVisibility(View.GONE);

                                List<User> userList = response.body().getData();
                                userAdapter = new UserAdapter(userList, R.layout.item_user, MainAdminActivity.this);
                                rvUserList.setAdapter(userAdapter);
                                userAdapter.notifyDataSetChanged();

                            } else {
                                rvUserList.setVisibility(View.GONE);
                                lblDataNA.setVisibility(View.VISIBLE);
                            }
                        } else {
                            GetAllUserResponse getAllUserResponse = gson.fromJson(response.errorBody().charStream(), getUserResponseType);

                            loading.setVisibility(View.GONE);
                            rvUserList.setVisibility(View.GONE);
                            lblDataNA.setVisibility(View.VISIBLE);

                            Toast.makeText(MainAdminActivity.this, getAllUserResponse.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetAllUserResponse> call, Throwable t) {
                        t.printStackTrace();

                        loading.setVisibility(View.GONE);
                        rvUserList.setVisibility(View.GONE);
                        lblDataNA.setVisibility(View.VISIBLE);

                        Toast.makeText(MainAdminActivity.this, "Failed to populate users data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            loading.setVisibility(View.GONE);
            rvUserList.setVisibility(View.GONE);
            lblDataNA.setVisibility(View.VISIBLE);

            Toast.makeText(this, "Failed to populate users data", Toast.LENGTH_LONG).show();
        }
    }
}
