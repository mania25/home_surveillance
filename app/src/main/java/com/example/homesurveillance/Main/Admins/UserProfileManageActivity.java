package com.example.homesurveillance.Main.Admins;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileManageActivity extends AppCompatActivity {
    private EditText lblUsername, txtPassword, txtFullname;
    private Spinner optRole;
    private Button btnSave, btnUpdate, btnDelete, btnCancel;

    private SharedPreferenceUtils userSharedPreference;

    private Gson gson;
    private Type type;

    private User user;
    private ProgressDialog dialog;
    private Intent getIntent;

    private String[] itemsRole = new String[]{"Admin", "Home Owner", "Security"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_user);

        lblUsername = (EditText)findViewById(R.id.lblUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        txtFullname = (EditText)findViewById(R.id.txtFullname);

        optRole = (Spinner)findViewById(R.id.optRole);

        btnSave = (Button)findViewById(R.id.btnSave);
        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnDelete = (Button)findViewById(R.id.btnDelete);
        btnCancel = (Button)findViewById(R.id.btnCancel);

        getIntent = getIntent();
        userSharedPreference = new SharedPreferenceUtils(UserProfileManageActivity.this, "user");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsRole);

        optRole.setAdapter(adapter);

        gson = new Gson();
        type = new TypeToken<User>() {}.getType();

        if (getIntent.hasExtra("userDetails")) {
            lblUsername.setEnabled(false);
            btnSave.setVisibility(View.GONE);
            btnUpdate.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);

            user = gson.fromJson(getIntent.getExtras().getString("userDetails"), type);

            lblUsername.setText(user.getUsername());
            txtFullname.setText(user.getFullname());

            for (int adapterIndex = 0; adapterIndex < adapter.getCount(); adapterIndex++) {
                if (user.getRole().trim().equals(adapter.getItem(adapterIndex).toString())) {
                    optRole.setSelection(adapterIndex);
                    break;
                }
            }

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = ProgressDialog.show(UserProfileManageActivity.this, "",
                            "Loading. Please wait...", true);

                    updateUserRequest();
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileManageActivity.this);
                    builder.setTitle("Apakah Anda Yakin ?");
                    builder.setMessage("Anda akan menghapus data ini, apakah anda ingin melanjutkan ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteUserRequest();
                        }
                    });

                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.show();
                }
            });

        } else {
            lblUsername.setEnabled(true);
            btnSave.setVisibility(View.VISIBLE);
            btnUpdate.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = ProgressDialog.show(UserProfileManageActivity.this, "",
                            "Loading. Please wait...", true);

                    createUserRequest();
                }
            });
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void createUserRequest() {
        try {
            lblUsername.setError(null);
            txtPassword.setError(null);
            txtFullname.setError(null);

            String username = lblUsername.getText().toString();
            String password = txtPassword.getText().toString();
            final String fullName = txtFullname.getText().toString();
            String role = optRole.getSelectedItem().toString();

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(username)) {
                lblUsername.setError(getString(R.string.error_field_required));
                focusView = lblUsername;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(password)) {
                txtPassword.setError(getString(R.string.error_field_required));
                focusView = txtPassword;
                cancel = !cancel;
            }

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                txtPassword.setError(getString(R.string.error_invalid_password));
                focusView = txtPassword;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(fullName)) {
                txtFullname.setError(getString(R.string.error_field_required));
                focusView = txtFullname;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(role)) {
                Toast.makeText(this, "Role cannot be empty", Toast.LENGTH_SHORT).show();
                focusView = optRole;
                cancel = !cancel;
            }

            if (cancel) {
                focusView.requestFocus();

                dialog.dismiss();
            } else {
                Call<DefaultResponse> createUser = null;

                if (userSharedPreference.contains("AT")) {
                    createUser = RetrofitServiceUtils.sendAdminRequest().addUser(
                            "Bearer " + userSharedPreference.getPreferenceData("AT"),
                            username, password, fullName, optRole.getSelectedItem().toString());

                    createUser.enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            if (response.isSuccessful()) {
                                dialog.dismiss();

                                Toast.makeText(UserProfileManageActivity.this, "User has been created", Toast.LENGTH_LONG).show();

                                finish();
                            } else {
                                dialog.dismiss();

                                Toast.makeText(UserProfileManageActivity.this, "Failed to create user data", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            t.printStackTrace();

                            dialog.dismiss();

                            Toast.makeText(UserProfileManageActivity.this, "Failed to create user data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            dialog.dismiss();

            Toast.makeText(this, "Failed to create user data", Toast.LENGTH_LONG).show();
        }
    }

    private void updateUserRequest() {
        try {
            txtPassword.setError(null);
            txtFullname.setError(null);

            String password = txtPassword.getText().toString();
            final String fullName = txtFullname.getText().toString();
            String role = optRole.getSelectedItem().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                txtPassword.setError(getString(R.string.error_invalid_password));
                focusView = txtPassword;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(fullName)) {
                txtFullname.setError(getString(R.string.error_field_required));
                focusView = txtFullname;
                cancel = !cancel;
            }

            if (TextUtils.isEmpty(role)) {
                Toast.makeText(this, "Role cannot be empty", Toast.LENGTH_SHORT).show();
                focusView = optRole;
                cancel = !cancel;
            }

            if (cancel) {
                focusView.requestFocus();
                dialog.dismiss();
            } else {
                Call<DefaultResponse> updateUser = null;

                if (userSharedPreference.contains("AT")) {
                    updateUser = RetrofitServiceUtils.sendAdminRequest().updateUser(
                            "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId(),
                            password, fullName, optRole.getSelectedItem().toString());

                    updateUser.enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            if (response.isSuccessful()) {
                                dialog.dismiss();

                                Toast.makeText(UserProfileManageActivity.this, "User has been updated", Toast.LENGTH_LONG).show();

                                finish();
                            } else {
                                dialog.dismiss();

                                Toast.makeText(UserProfileManageActivity.this, "Failed to update user data", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            t.printStackTrace();

                            dialog.dismiss();

                            Toast.makeText(UserProfileManageActivity.this, "Failed to update user data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            dialog.dismiss();

            Toast.makeText(this, "Failed to update data", Toast.LENGTH_LONG).show();
        }
    }

    private void deleteUserRequest() {
        try {
            dialog = ProgressDialog.show(UserProfileManageActivity.this, "",
                    "Loading. Please wait...", true);

            Call<DefaultResponse> deleteUser = null;

            if (userSharedPreference.contains("AT")) {
                deleteUser = RetrofitServiceUtils.sendAdminRequest().deleteUser(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId());

                deleteUser.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        if (response.isSuccessful()) {
                            dialog.dismiss();

                            Toast.makeText(UserProfileManageActivity.this, "User has been deleted", Toast.LENGTH_LONG).show();

                            finish();
                        } else {
                            dialog.dismiss();

                            Toast.makeText(UserProfileManageActivity.this, "Failed to delete user data", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        t.printStackTrace();

                        dialog.dismiss();

                        Toast.makeText(UserProfileManageActivity.this, "Failed to delete user data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            dialog.dismiss();

            Toast.makeText(this, "Failed to delete user data", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}
