package com.example.homesurveillance.Main.Security;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homesurveillance.Main.LoginActivity;
import com.example.homesurveillance.Main.ProfileActivity;
import com.example.homesurveillance.Main.Security.Adapter.EventAdapter;
import com.example.homesurveillance.Models.Events;
import com.example.homesurveillance.Models.GetEventResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainSecurityActivity extends AppCompatActivity {
    private ProgressBar loading;
    private RecyclerView rvEventList;
    private TextView lblDataNA;

    private SharedPreferenceUtils userSharedPreference;

    private Gson gson;
    private Type userProfileType, getEventResponseType;
    private User user;

    private EventAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_security);

        loading = (ProgressBar)findViewById(R.id.loading);
        rvEventList = (RecyclerView)findViewById(R.id.rvEventList);
        lblDataNA = (TextView)findViewById(R.id.lblDataNA);

        rvEventList.setHasFixedSize(true);
        rvEventList.setLayoutManager(new LinearLayoutManager(this));
        userSharedPreference = new SharedPreferenceUtils(this, "user");

        gson = new Gson();

        userProfileType = new TypeToken<User>(){}.getType();
        getEventResponseType = new TypeToken<GetEventResponse>(){}.getType();

        FirebaseMessaging.getInstance().subscribeToTopic("security");

        getSecurityEventRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_owner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_user_logout:
                userSharedPreference.clearAllData();
                FirebaseMessaging.getInstance().unsubscribeFromTopic("security");

                Intent nextIntent = new Intent(MainSecurityActivity.this, LoginActivity.class);
                startActivity(nextIntent);

                finish();
                break;
            case R.id.action_user_profile:
                nextIntent = new Intent(MainSecurityActivity.this, ProfileActivity.class);
                startActivity(nextIntent);

                break;
        }
        return true;
    }

    private void getSecurityEventRequest() {
        try {
            loading.setVisibility(View.VISIBLE);
            rvEventList.setVisibility(View.GONE);

            Call<GetEventResponse> getEventResponseCall = null;

            user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), userProfileType);

            if (userSharedPreference.contains("AT")) {
                getEventResponseCall = RetrofitServiceUtils.sendSecurityRequest().getSecurityEvent(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId());

                getEventResponseCall.enqueue(new Callback<GetEventResponse>() {
                    @Override
                    public void onResponse(Call<GetEventResponse> call, Response<GetEventResponse> response) {
                        loading.setVisibility(View.GONE);

                        if (response.isSuccessful()) {
                            if (response.body().getData().size() > 0) {
                                rvEventList.setVisibility(View.VISIBLE);
                                lblDataNA.setVisibility(View.GONE);

                                List<Events> eventsList = response.body().getData();
                                eventAdapter = new EventAdapter(eventsList, R.layout.item_event, MainSecurityActivity.this);
                                rvEventList.setAdapter(eventAdapter);
                                eventAdapter.notifyDataSetChanged();

                            } else {
                                rvEventList.setVisibility(View.GONE);
                                lblDataNA.setVisibility(View.VISIBLE);
                            }
                        } else {
                            GetEventResponse eventResponse = gson.fromJson(response.errorBody().charStream(), getEventResponseType);

                            loading.setVisibility(View.GONE);
                            rvEventList.setVisibility(View.GONE);
                            lblDataNA.setVisibility(View.VISIBLE);

                            Toast.makeText(MainSecurityActivity.this, eventResponse.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetEventResponse> call, Throwable t) {
                        t.printStackTrace();

                        loading.setVisibility(View.GONE);
                        rvEventList.setVisibility(View.GONE);
                        lblDataNA.setVisibility(View.VISIBLE);

                        Toast.makeText(MainSecurityActivity.this, "Failed to populate event data", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            loading.setVisibility(View.GONE);
            rvEventList.setVisibility(View.GONE);
            lblDataNA.setVisibility(View.VISIBLE);

            Toast.makeText(this, "Failed to populate event data", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("MyData")
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSecurityEventRequest();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("onReceive: ", intent.getExtras().getString("type"));

            getSecurityEventRequest();
        }
    };

}
