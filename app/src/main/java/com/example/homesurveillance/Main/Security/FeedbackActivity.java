package com.example.homesurveillance.Main.Security;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {
    private EditText txtResponseContent;
    private Button btnSubmit;

    private SharedPreferenceUtils userSharedPreference;
    private ProgressDialog dialog;
    private String eventId;

    private Gson gson;
    private Type userProfileType;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        txtResponseContent = (EditText)findViewById(R.id.txtResponseContent);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        Intent intent = getIntent();

        eventId = intent.getExtras().getString("eventId");

        userSharedPreference = new SharedPreferenceUtils(FeedbackActivity.this, "user");

        gson = new Gson();
        userProfileType = new TypeToken<User>(){}.getType();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });
    }

    private void sendFeedback() {
        try {
            user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), userProfileType);

            txtResponseContent.setError(null);

            dialog = ProgressDialog.show(FeedbackActivity.this, "",
                    "Loading. Please wait...", true);

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(txtResponseContent.getText().toString())) {
                txtResponseContent.setError(getString(R.string.error_field_required));
                focusView = txtResponseContent;
                cancel = !cancel;
            }

            if (cancel) {
                dialog.dismiss();

                focusView.requestFocus();
            } else {
                Call<DefaultResponse> createFeedbackCall = null;

                if (userSharedPreference.contains("AT")) {
                    createFeedbackCall = RetrofitServiceUtils.sendSecurityRequest().createFeedback(
                            "Bearer " + userSharedPreference.getPreferenceData("AT"), eventId,
                            user.getId(), txtResponseContent.getText().toString()
                    );

                    createFeedbackCall.enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            dialog.dismiss();

                            if (response.isSuccessful()) {
                                Toast.makeText(FeedbackActivity.this, "Feedback has been submitted", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(FeedbackActivity.this, "Failed to submit feedback", Toast.LENGTH_SHORT).show();
                            }

                            finish();
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            dialog.dismiss();

                            Toast.makeText(FeedbackActivity.this, "Failed to submit feedback", Toast.LENGTH_SHORT).show();

                            finish();

                            t.printStackTrace();
                        }
                    });
                }
            }
        } catch (Exception ex) {
            dialog.dismiss();

            Toast.makeText(FeedbackActivity.this, "Failed to submit feedback", Toast.LENGTH_SHORT).show();

            finish();

            ex.printStackTrace();
        }
    }
}
