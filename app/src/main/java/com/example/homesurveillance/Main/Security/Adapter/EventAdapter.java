package com.example.homesurveillance.Main.Security.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.homesurveillance.Main.Security.DetailActivity;
import com.example.homesurveillance.Main.Security.FeedbackActivity;
import com.example.homesurveillance.Models.Events;
import com.example.homesurveillance.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {
    private List<Events> eventsList;
    private int rowLayout;
    private Context context;

    private Gson gson;
    private Type eventType;

    public static class EventViewHolder extends RecyclerView.ViewHolder {
        TextView lblIssuerName, lblIssuedDate, lblStatus;
        Button btnGiveResponse, btnDetail;

        public EventViewHolder(View itemView) {
            super(itemView);

            lblIssuerName = (TextView)itemView.findViewById(R.id.lblIssuerName);
            lblIssuedDate = (TextView)itemView.findViewById(R.id.lblIssuedDate);
            lblStatus = (TextView)itemView.findViewById(R.id.lblStatus);

            btnGiveResponse = (Button)itemView.findViewById(R.id.btnGiveResponse);
            btnDetail = (Button)itemView.findViewById(R.id.btnDetail);
        }
    }

    public EventAdapter(List<Events> eventsList, int rowLayout, Context context) {
        this.eventsList = eventsList;
        this.rowLayout = rowLayout;
        this.context = context;

        gson = new Gson();
        eventType = new TypeToken<Events> (){}.getType();
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, final int position) {
        holder.lblIssuerName.setText(eventsList.get(position).getIssuerUser().getFullname());
        holder.lblIssuedDate.setText(eventsList.get(position).getCreatedAt());

        int status = eventsList.get(position).getStatus();

        if (status == 0) {
            holder.lblStatus.setText("Not Handled");

            holder.btnDetail.setVisibility(View.GONE);
            holder.btnGiveResponse.setVisibility(View.VISIBLE);
        } else if (status == 1) {
            holder.lblStatus.setText("On Going");

            holder.btnDetail.setVisibility(View.VISIBLE);
            holder.btnGiveResponse.setVisibility(View.GONE);
        } else if (status == 2) {
            holder.lblStatus.setText("Finished");

            holder.btnDetail.setVisibility(View.GONE);
            holder.btnGiveResponse.setVisibility(View.GONE);
        } else if (status == 3) {
            holder.lblStatus.setText("Cancelled");

            holder.btnDetail.setVisibility(View.GONE);
            holder.btnGiveResponse.setVisibility(View.GONE);
        }

        holder.btnGiveResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent feedbackIntent = new Intent(context, FeedbackActivity.class);

                feedbackIntent.putExtra("eventId", eventsList.get(position).getId());

                ((Activity)context).startActivity(feedbackIntent);
            }
        });

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra("eventData", gson.toJson(eventsList.get(position), eventType));

                ((Activity)context).startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }
}
