package com.example.homesurveillance.Main.Users;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homesurveillance.Main.LoginActivity;
import com.example.homesurveillance.Main.ProfileActivity;
import com.example.homesurveillance.Models.CreateEventResponse;
import com.example.homesurveillance.Models.DefaultResponse;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.RetrofitServiceUtils;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements IVLCVout.Callback {
    public final static String TAG = "MainActivity";
    private String mFilePath;
    private LinearLayout layoutTimer, layoutPanicButton;
    private TextView lblTimeRemaining;
    private SurfaceView mSurface;
    private SurfaceHolder holder;
    private LibVLC libvlc;
    private ImageView btnImgPanic;
    private ProgressDialog dialog;

    private SharedPreferenceUtils userSharedPreference;
    private MediaPlayer mMediaPlayer = null;

    private int mVideoWidth;
    private int mVideoHeight;

    private User user;
    private Gson gson;
    private Type type;

    private CountDownTimer cTimer = null;

    private static final String FORMAT = "%02d:%02d:%02d";

    private String eventID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);

        try {
            userSharedPreference = new SharedPreferenceUtils(MainActivity.this, "user");

            gson = new Gson();
            type = new TypeToken<User>() {}.getType();

            user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), type);

            layoutTimer = (LinearLayout)findViewById(R.id.layoutTimer);
            lblTimeRemaining = (TextView)findViewById(R.id.lblTimeRemaining);

            layoutPanicButton = (LinearLayout)findViewById(R.id.layoutPanicButton);
            btnImgPanic = (ImageView)findViewById(R.id.btnImgPanic);
            mSurface = (SurfaceView) findViewById(R.id.videoStreamRaspi);

            holder = mSurface.getHolder();

            if (user.getDevice() != null) {
                mFilePath = "rtsp://" + user.getDevice().getAddress() + ":8554/" + user.getDevice().getSerialNo();

                Log.d(TAG, "Playing: " + mFilePath);
            }

            btnImgPanic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = ProgressDialog.show(MainActivity.this, "",
                            "Loading. Please wait...", true);

                    executePanicButton();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void executePanicButton() {
        try {
            Call<CreateEventResponse> createEventResponseCall = null;

            if (userSharedPreference.contains("AT")) {
                createEventResponseCall = RetrofitServiceUtils.sendUserRequest().createEvent(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), user.getId()
                );

                createEventResponseCall.enqueue(new Callback<CreateEventResponse>() {
                    @Override
                    public void onResponse(Call<CreateEventResponse> call, Response<CreateEventResponse> response) {
                        dialog.dismiss();

                        if (response.isSuccessful()) {
                            layoutTimer.setVisibility(View.VISIBLE);
                            layoutPanicButton.setVisibility(View.GONE);

                            eventID = response.body().getData().getId();

                            startTimer();
                        } else {
                            layoutTimer.setVisibility(View.GONE);
                            layoutPanicButton.setVisibility(View.VISIBLE);

                            Toast.makeText(MainActivity.this, "Failed to emit event", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CreateEventResponse> call, Throwable t) {
                        dialog.dismiss();

                        layoutTimer.setVisibility(View.GONE);
                        layoutPanicButton.setVisibility(View.VISIBLE);

                        Toast.makeText(MainActivity.this, "Failed to emit event", Toast.LENGTH_LONG).show();

                        t.printStackTrace();
                    }
                });
            }
        } catch (Exception ex) {
            dialog.dismiss();

            layoutTimer.setVisibility(View.GONE);
            layoutPanicButton.setVisibility(View.VISIBLE);

            Toast.makeText(MainActivity.this, "Failed to emit event", Toast.LENGTH_LONG).show();

            ex.printStackTrace();
        }
    }

    private void executeExpiredPanicButton() {
        try {
            dialog.show();

            Call<DefaultResponse> defaultResponseCall = null;

            if (userSharedPreference.contains("AT")) {
                defaultResponseCall = RetrofitServiceUtils.sendUserRequest().updateEvent(
                        "Bearer " + userSharedPreference.getPreferenceData("AT"), eventID, 3
                );

                defaultResponseCall.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        dialog.dismiss();

                        if (response.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Your request has expired.", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();

                            Toast.makeText(MainActivity.this, "Failed to update event", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        dialog.dismiss();

                        Toast.makeText(MainActivity.this, "Failed to update event", Toast.LENGTH_LONG).show();

                        t.printStackTrace();
                    }
                });
            }
        } catch (Exception ex) {
            dialog.dismiss();

            Toast.makeText(MainActivity.this, "Failed to update event", Toast.LENGTH_LONG).show();

            ex.printStackTrace();
        }
    }

    //start timer function
    private void startTimer() {
        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                lblTimeRemaining.setText("" + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                layoutTimer.setVisibility(View.GONE);
                layoutPanicButton.setVisibility(View.VISIBLE);

                executeExpiredPanicButton();
            }
        };
        cTimer.start();
    }

    //cancel timer
    private void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("MyData")
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("onReceive: ", intent.getExtras().getString("type"));

            if (intent.getExtras().getString("type").equals("event_accept")) {
                cancelTimer();

                layoutTimer.setVisibility(View.GONE);
                layoutPanicButton.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_user_logout:
                userSharedPreference.clearAllData();

                Intent nextIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(nextIntent);

                finish();
                break;
            case R.id.action_user_profile:
                nextIntent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(nextIntent);

                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_owner, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            mSurface = (SurfaceView) findViewById(R.id.videoStreamRaspi);
            holder = mSurface.getHolder();

            user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), type);

            if (user.getDevice() != null) {
                mFilePath = "rtsp://" + user.getDevice().getAddress() + ":8554/" + user.getDevice().getSerialNo();

                mSurface.setVisibility(View.VISIBLE);

                createPlayer(mFilePath);

                Log.d(TAG, "Playing: " + mFilePath);
            } else {
                mSurface.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        cancelTimer();
        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancelTimer();
        releasePlayer();
    }

    /**
     * Used to set size for SurfaceView
     *
     * @param width
     * @param height
     */
    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        if (holder == null || mSurface == null)
            return;

        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;

        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);

        holder.setFixedSize(mVideoWidth, mVideoHeight);
        LayoutParams lp = mSurface.getLayoutParams();
        lp.width = w;
        lp.height = h;
        mSurface.setLayoutParams(lp);
        mSurface.invalidate();
    }

    /**
     * Creates MediaPlayer and plays video
     *
     * @param media
     */
    private void createPlayer(String media) {
        releasePlayer();
        try {
            if (media.length() > 0) {
                Toast toast = Toast.makeText(this, media, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0,
                        0);
                toast.show();
            }

            // Create LibVLC
            // TODO: make this more robust, and sync with audio demo
            ArrayList<String> options = new ArrayList<String>();
            //options.add("--subsdec-encoding <encoding>");
            options.add("--aout=opensles");
            options.add("--audio-time-stretch"); // time stretching
            options.add("-vvv"); // verbosity
            libvlc = new LibVLC(this, options);
            holder.setKeepScreenOn(true);

            // Creating media player
            mMediaPlayer = new MediaPlayer(libvlc);
            mMediaPlayer.setEventListener(mPlayerListener);

            // Seting up video output
            final IVLCVout vout = mMediaPlayer.getVLCVout();
            vout.setVideoView(mSurface);
            //vout.setSubtitlesView(mSurfaceSubtitles);
            vout.addCallback(this);
            vout.attachViews();

            Media m = new Media(libvlc, Uri.parse(media));
            mMediaPlayer.setMedia(m);
            mMediaPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error in creating player!", Toast
                    .LENGTH_LONG).show();
        }
    }

    private void releasePlayer() {
        if (libvlc == null || mMediaPlayer == null)
            return;

        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = null;

        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    /**
     * Registering callbacks
     */
    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);

    @Override
    public void onNewLayout(IVLCVout vout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vout) {
    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        Log.e(TAG, "Error with hardware acceleration");
        this.releasePlayer();
        Toast.makeText(this, "Error with hardware acceleration", Toast.LENGTH_LONG).show();
    }

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<MainActivity> mOwner;

        public MyPlayerListener(MainActivity owner) {
            mOwner = new WeakReference<MainActivity>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            MainActivity player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    Log.d(TAG, "MediaPlayerEndReached");
                    player.releasePlayer();
                    break;
                case MediaPlayer.Event.Playing:
                case MediaPlayer.Event.Paused:
                case MediaPlayer.Event.Stopped:
                default:
                    break;
            }
        }
    }
}