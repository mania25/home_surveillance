package com.example.homesurveillance.Main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homesurveillance.Main.Admins.MainAdminActivity;
import com.example.homesurveillance.Main.Security.MainSecurityActivity;
import com.example.homesurveillance.Main.Users.MainActivity;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.example.homesurveillance.Utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SplashScreenActivity extends AppCompatActivity {
    private TextView lblAppVersion;
    private static int SPLASH_TIME_OUT = 3000;

    private SharedPreferenceUtils userSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        lblAppVersion = (TextView)findViewById(R.id.lblAppVersion);

        try {
            userSharedPreference = new SharedPreferenceUtils(SplashScreenActivity.this, "user");

            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            lblAppVersion.setText(version);

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    ActivityCompat.requestPermissions(SplashScreenActivity.this,
                            new String[]{Manifest.permission.INTERNET},
                            1);
                }
            }, SPLASH_TIME_OUT);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                try {
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Intent nextIntent = null;

                        if (userSharedPreference.contains("userProfile")){
                            Gson gson = new Gson();
                            Type type = new TypeToken<User>() {}.getType();

                            User user = gson.fromJson(userSharedPreference.getPreferenceData("userProfile"), type);

                            if (user.getRole().equals("Admin")) {
                                nextIntent = new Intent(SplashScreenActivity.this, MainAdminActivity.class);
                            } else if (user.getRole().equals("Security")) {
                                nextIntent = new Intent(SplashScreenActivity.this, MainSecurityActivity.class);
                            } else if (user.getRole().equals("Home Owner")) {
                                nextIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                            }
                        } else {
                            nextIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        }

                        startActivity(nextIntent);

                        // close this activity
                        finish();
                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                        Toast.makeText(SplashScreenActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
