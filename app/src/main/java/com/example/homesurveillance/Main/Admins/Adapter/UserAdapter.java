package com.example.homesurveillance.Main.Admins.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.homesurveillance.Main.Admins.UserProfileManageActivity;
import com.example.homesurveillance.Models.User;
import com.example.homesurveillance.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private List<User> userList;
    private int rowLayout;
    private Context context;

    private Gson gson;
    private Type userType;

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView lblFullName, lblCreatedDate, lblRole;
        Button btnEditProfile;

        public UserViewHolder(View itemView) {
            super(itemView);

            lblFullName = (TextView)itemView.findViewById(R.id.lblFullName);
            lblCreatedDate = (TextView)itemView.findViewById(R.id.lblCreatedDate);
            lblRole = (TextView)itemView.findViewById(R.id.lblRole);

            btnEditProfile = (Button)itemView.findViewById(R.id.btnEditProfile);
        }
    }

    public UserAdapter(List<User> userList, int rowLayout, Context context) {
        this.userList = userList;
        this.rowLayout = rowLayout;
        this.context = context;

        gson = new Gson();
        userType = new TypeToken<User>(){}.getType();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, final int position) {
        holder.lblFullName.setText(userList.get(position).getFullname());
        holder.lblCreatedDate.setText(userList.get(position).getCreatedAt());
        holder.lblRole.setText(userList.get(position).getRole());

        holder.btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editProfileIntent = new Intent(context, UserProfileManageActivity.class);

                editProfileIntent.putExtra("userDetails", gson.toJson(userList.get(position), userType));

                ((Activity)context).startActivity(editProfileIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
