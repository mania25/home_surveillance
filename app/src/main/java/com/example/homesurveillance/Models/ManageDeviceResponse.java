package com.example.homesurveillance.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ManageDeviceResponse extends DefaultResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private boolean data;

    public boolean getData() {
        return data;
    }

    public void setData(boolean user) {
        this.data = user;
    }
}
