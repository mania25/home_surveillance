
package com.example.homesurveillance.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserLoginResponse extends DefaultResponse implements Serializable
{
    @SerializedName("data")
    @Expose
    private User data;
    private final static long serialVersionUID = 615074282340436360L;

    public User getData() {
        return data;
    }

    public void setData(User user) {
        this.data = user;
    }

}
