package com.example.homesurveillance.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreateEventResponse extends DefaultResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private Events data;
    private final static long serialVersionUID = 615074282340436360L;

    public Events getData() {
        return data;
    }

    public void setData(Events user) {
        this.data = user;
    }
}
