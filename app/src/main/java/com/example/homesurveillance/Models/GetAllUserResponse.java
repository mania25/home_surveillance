package com.example.homesurveillance.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetAllUserResponse extends DefaultResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private List<User> data;
    private final static long serialVersionUID = 615074282340436360L;

    public List<User> getData() {
        return data;
    }

    public void setData(List<User> user) {
        this.data = user;
    }
}
