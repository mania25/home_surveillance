package com.example.homesurveillance.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetEventResponse extends DefaultResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private List<Events> data;
    private final static long serialVersionUID = 615074282340436360L;

    public List<Events> getData() {
        return data;
    }

    public void setData(List<Events> user) {
        this.data = user;
    }
}
